#! /bin/bash

#----------------------------------------------------#
#                                                    #
#                 Exercise 1.3.1.3                   #
#                                                    #
#    Bash ping sweep for the 10.11.1.0/24 network    #
#                                                    #
#----------------------------------------------------#


for ip in $(seq 1 254);
do
	ping -c 1 10.11.1.$ip | grep "64 bytes" | cut -d" " -f 4 | cut -d":" -f1 &
done

