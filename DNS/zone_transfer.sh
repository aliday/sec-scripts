#!/bin/bash

#
#
# Zone Transfer script
#
#

echo "Simple Zone Transfer Script"
echo "---------------------------"
echo ""
if [ -z "$1" ]; then
	echo "[*] Usage: $0 <domain name>"
	exit 0
fi
echo "[*] Target is: " $1
echo ""

for server in $(host -t ns $1 | cut -d " " -f 4); do
	echo "[*] Name server found: " $server
	host -l $1 $server | grep "has address"
done

